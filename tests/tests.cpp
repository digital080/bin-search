#include "catch.hpp"
#include "../src/search.hpp"
#include <vector>
#include <string>


using namespace std;



TEST_CASE("TEST TO PASS", "[test1]")
{
  vector<int> D1 = {1, 2, 3, 4, 5};
  vector<int> D2 = {};

  CHECK( binarySearch(D1 , 4) == 3); //Vector, index 4 is = 3
  
  CHECK( binarySearch(D1 , 3) == 2);
  
  CHECK( binarySearch(D1 , 5) == 4);
  

 
 //TEST TO FAIL
 
  CHECK( binarySearch(D2 , 0) == 0); //Empty vector
 // CHECK( binarySearch(D1 , 5) == 2*2); //Compare with char 
  
  
  //CHECK( binarySearch(D2 , 3) == 1);
}

